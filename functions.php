<?php
// hide top admin bar
    add_filter('show_admin_bar','__return_false');

// for menu
    function register_primary_menu() {
        register_nav_menu( 'header_top_menu', __( 'Primary Menu', 'startup' ) );
    }
    add_action( 'after_setup_theme', 'register_primary_menu' );

// for support thumbnail
add_theme_support('post-thumbnails', array('post', 'page'));
set_post_thumbnail_size(465, 260, true);

// for logo
add_theme_support( 'custom-logo', array(
    'height'      => 28,
    'width'       => 131,
    'flex-height' => true,
    'flex-width'  => true,
    'header-text' => array( 'site-title', 'site-description' ),
) );
add_action( 'after_setup_theme', 'add_theme_support' );

// for post length
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 99 );



// Register Custom Post
add_action('init', 'special_post');
function special_post(){
    register_post_type('special-post-items',
        array(
            'labels' => array(
                'name'              => __('Special Post'),
                'singular_name'     => __('Special_post'),
                'menu_name'         => __('Special Post'),
                'name_admin_bar'    => __('Special Post'),
                'all_items'         => __('All Special Post'),
                'add_new'           => __('Add Special Post'),
                'add_new_item'      => __('Add Special Post'),
                'edit_item'         => __('Edit Special Post'),
                'new_item'          => __('New Special Post'),
                'view_item'         => __('View Special Post'),
                'search_items'       => __('Search Special Post')
            ),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'slider-item'),
            'menu_position' => 8,
            'menu_icon'     => 'dashicons-admin-customizer',
            'supports'      => array('title', 'thumbnail', 'editor')
        )
    );
    register_post_type('testimonial',
        array(
            'labels' => array(
                'name'              => __('Testimoniul'),
                'singular_name'     => __('Testimoniul'),
                'menu_name'         => __('Testimoniul'),
                'name_admin_bar'    => __('Testimoniul'),
                'all_items'         => __('All Testimoniul'),
                'add_new'           => __('Add Testimoniul'),
                'add_new_item'      => __('Add Testimoniul'),
                'edit_item'         => __('Edit Testimoniul'),
                'new_item'          => __('New Testimoniul'),
                'view_item'         => __('View Testimoniul'),
                'search_items'       => __('Search Testimoniul')
            ),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'slider-item'),
            'menu_position' => 8,
            'menu_icon'     => 'dashicons-format-quote',
            'supports'      => array('title', 'thumbnail', 'editor')
        )
    );
}

// Enable Custom post taxonomy for category
function special_post_taxonomy(){
    register_taxonomy(
        'slider_cat', 'special-post-items',
        array(
            'hierarchical'  => true,
            'label'         => 'Special Post Category',
            'query_var'     =>true,
            'rewrite'       =>  array(
                'slug'  => 'special-post-category',
                'with_front'    => true
            )
        )
    );
    register_taxonomy(
        'slider_cat', 'testimonial',
        array(
            'hierarchical'  => true,
            'label'         => 'Special Post Category',
            'query_var'     =>true,
            'rewrite'       =>  array(
                'slug'  => 'special-post-category',
                'with_front'    => true
            )
        )
    );
}
add_action('init', 'special_post_taxonomy');

// Custom Metabox
function special_post_custom_meta(){
    add_meta_box('slider_meta', __('Others Section', 'startup'), 'slider_meta_callback', 'special-post-items');
}
add_action('add_meta_boxes', 'special_post_custom_meta');

// Custom Metabox
function testimonial_custom_meta(){
    add_meta_box('testimonial_meta', __('Others Section', 'startup'), 'testimonial_meta_callback', 'testimonial');
}
add_action('add_meta_boxes', 'testimonial_custom_meta');

// field
function slider_meta_callback($post){
    wp_nonce_field(basename(__FILE__), 'slider_nonce');
    $slider_stored_meta = get_post_meta($post->ID);
    ?>
    <input type="text" name="meta-subtitle-slider" value="<?php  if(isset($slider_stored_meta['meta-subtitle-slider'])) echo $slider_stored_meta['meta-subtitle-slider'][0]; ?>" style="width:100%; font-size:16px;" placeholder="Enter Sub Title Here (Red)">
    <input type="text" name="meta-subtitle-slider-black" value="<?php  if(isset($slider_stored_meta['meta-subtitle-slider-black'])) echo $slider_stored_meta['meta-subtitle-slider-black'][0]; ?>" style="width:100%; font-size:16px;" placeholder="Enter Sub Title Here (black)">
    <input type="text" name="youtube-link" value="<?php  if(isset($slider_stored_meta['youtube-link'])) echo $slider_stored_meta['youtube-link'][0]; ?>" style="width:100%; font-size:16px;" placeholder="Enter Youtube Link Here">
    <?php
}
// field for testimonial
function testimonial_meta_callback($post){
    wp_nonce_field(basename(__FILE__), 'slider_nonce');
    $slider_stored_meta = get_post_meta($post->ID);
    ?>
    <input type="text" name="designation" value="<?php  if(isset($slider_stored_meta['designation'])) echo $slider_stored_meta['designation'][0]; ?>" style="width:100%; font-size:16px;" placeholder="Enter designation">
    <?php
}


// Save Field Value
function slider_meta_save($post_id){
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['slider_nonce']) && wp_verify_nonce($_POST['slider_nonce'], basename(__FILE__)))? 'true' : 'false';

    //Exists script depending on save status
    if($is_autosave || $is_revision || !$is_valid_nonce){
        return;
    }
    // checks for input and sanitizes/save if needed
    if(isset($_POST['meta-subtitle-slider'])){
        update_post_meta($post_id, 'meta-subtitle-slider', sanitize_text_field($_POST['meta-subtitle-slider']));
        update_post_meta($post_id, 'meta-subtitle-slider-black', sanitize_text_field($_POST['meta-subtitle-slider-black']));
        update_post_meta($post_id, 'youtube-link', sanitize_text_field($_POST['youtube-link']));
    }
    if(isset($_POST['designation'])){
        update_post_meta($post_id, 'designation', sanitize_text_field($_POST['designation']));
    }
}
add_action('save_post', 'slider_meta_save');



// for set featured image to custom post
add_theme_support( 'post-thumbnails' );
function theme_setup() {
    register_post_type( 'slider_cat', array(
        'supports' => array('title', 'thumbnail'),
    ));
}
add_action( 'after_setup_theme', 'theme_setup' );

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

function get_custom_cat_ID( $cat_name ) {
    $cat = get_term_by( 'name', $cat_name, 'testimonial' );
    if ( $cat ) {
        return $cat->term_id;
    }
    return 0;
}


// Removing customizer from appearance
function remove_customize_page(){
    global $submenu;
    unset($submenu['themes.php'][6]);
}
add_action('admin_menu', 'remove_customize_page');

// Adding Customizer into Menu
function register_menu_item_for_customizeer(){
    add_menu_page('Customizer title', 'Theme Options', 'manage_options', 'customize.php', '', '', 100);
}
add_action('admin_menu', 'register_menu_item_for_customizeer');

// Customizer Main
function my_customizer($wp_customize){
    $wp_customize->remove_section('nav');
    $wp_customize->remove_section('static_front_page');
    $wp_customize->remove_section('title_tagline');
    $wp_customize->remove_section('widgets');

    /* ------------------- Panel Header ---------------------- */
    $wp_customize->add_panel('panel_1', array(
        'priority' => 9,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => __('Header', 'nsfw'),
        'description' => false,
    ));


    $wp_customize-> add_section('section_1', array(
        'title' => 'Logos',
        'priority' => 9,
        'panel' => 'panel_2'
    ));

    /* -------------- Header email ---------- */
    $wp_customize -> add_setting ('header_email', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'header_email', array(
            'section' => 'section_3',
            'label' => 'Write Header email',
            'type' => 'text'
        ))
    );

    /* \\\\\\\\\\\\\\\\\\\\\\\\\\ Logo Section ////////////////////////// */
    $wp_customize-> add_section('section_1', array(
        'title' => 'Logos',
        'priority' => 9,
        'panel' => 'panel_1'
    ));

    /* -------------- Settings and control ---------- */
    $wp_customize -> add_setting ('logo', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'logo', array(
            'section' => 'section_1',
            'label' => 'Upload Your Logo'
        ))
    );

    /* \\\\\\\\\\\\\\\\\\\\\\\\\\ Favicon Section ////////////////////////// */
    $wp_customize-> add_section('section_2', array(
        'title' => 'Favicon',
        'priority' => 6,
        'panel' => 'panel_1'
    ));

    /* -------------- Settings and control ---------- */
    $wp_customize -> add_setting ('favi', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'favi', array(
            'section' => 'section_2',
            'label' => 'Upload Your Favicon'
        ))
    );
    /* \\\\\\\\\\\\\\\\\\\\\\\\\\ Header Text Section ////////////////////////// */
    $wp_customize-> add_section('section_3', array(
        'title' => 'Header Text',
        'priority' => 7,
        'panel' => 'panel_1'
    ));

    /* -------------- Header Phone ---------- */
    $wp_customize -> add_setting ('header_phone', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'header_phone', array(
            'section' => 'section_3',
            'label' => 'Write Header Text',
            'type' => 'text'
        ))
    );

    /* -------------- Header email ---------- */
    $wp_customize -> add_setting ('header_email', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'header_email', array(
            'section' => 'section_3',
            'label' => 'Write Header email',
            'type' => 'text'
        ))
    );
    /* -------------- Footer address ---------- */
    $wp_customize -> add_setting ('footer_address', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'footer_address', array(
            'section' => 'section_3',
            'label' => 'Write Address',
            'type' => 'text'
        ))
    );



// social link
    $wp_customize->add_panel('panel_2', array(
        'priority' => 10,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => __('Social Link', 'nsfws'),
        'description' => false,
    ));



    $wp_customize-> add_section('section_4', array(
        'title' => 'Social Link',
        'priority' => 11,
        'panel' => 'panel_2'
    ));


    /* -------------- facebook page link ---------- */
    $wp_customize -> add_setting ('facebook', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'facebook', array(
            'section' => 'section_4',
            'label' => 'Write facebook page link with https://',
            'type' => 'text'
        ))
    );

    /* -------------- Twitter link ---------- */
    $wp_customize -> add_setting ('twitter', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'twitter', array(
            'section' => 'section_4',
            'label' => 'Write Twitter link with https://',
            'type' => 'text'
        ))
    );





// Copy Right
    $wp_customize->add_panel('panel_3', array(
        'priority' => 12,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => __('Copyright', 'nsfwsx'),
        'description' => false,
    ));



    $wp_customize-> add_section('section_5', array(
        'title' => 'copyright',
        'priority' => 13,
        'panel' => 'panel_3'
    ));


    /* -------------- facebook page link ---------- */
    $wp_customize -> add_setting ('copyright', array(
        'default' => '',
        'transport' => 'refresh'
    ));
    $wp_customize -> add_control(
        new WP_Customize_Image_Control($wp_customize,'copyright', array(
            'section' => 'section_5',
            'label' => 'copyright text',
            'type' => 'text'
        ))
    );

}
add_action('customize_register', 'my_customizer');


?>