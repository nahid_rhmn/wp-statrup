<?php get_header(); ?>



			<!-- Start we-offer Area -->
<?php
if( have_posts() ) : the_post(); ?>

			<section class="we-offer-area section-gap" id="offer">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content col-lg-10">
							<div class="title text-center">
								<h1 class="mb-10"><?php the_title(); ?></h1>
								<p><?php the_post_thumbnail('home_post', array('class' => 'post-thumb')); ?></p>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-lg-12">
							<div class="single-offer d-flex flex-row pb-30">
								<div class="desc">

										<?php the_content(); ?>

								</div>
							</div>
						</div>
					</div>
				</div>	
			</section>
			<!-- End we-offer Area -->

<?php endif; ?>


<?php get_footer(); ?>



