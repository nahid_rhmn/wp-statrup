<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title><?php bloginfo( 'name' ); ?></title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/linearicons.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/nice-select.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <style>
        .banner-area {

        }
        .video-area {

        }
        .confirm-radio input:checked + label {
            background: url(<?php echo get_template_directory_uri(); ?>/img/elements/success-radio.png) no-repeat center center/cover;
            border: none;
        }
        .disabled-radio input:checked + label {
            background: url(<?php echo get_template_directory_uri(); ?>/img/elements/disabled-radio.png) no-repeat center center/cover;
            border: none;
        }
        .callto-action-area {
            background: url(<?php echo get_template_directory_uri(); ?>/img/callaction-bg.jpg) center;
            background-size: cover;
        }
        .primary-checkbox input:checked + label {
            background: url(<?php echo get_template_directory_uri(); ?>/img/elements/primary-check.png) no-repeat center center/cover;
            border: none;
        }
        .confirm-checkbox input:checked + label {
            background: url(<?php echo get_template_directory_uri(); ?>/img/elements/success-check.png) no-repeat center center/cover;
            border: none;
        }
        .disabled-checkbox input:checked + label {
            background: url(<?php echo get_template_directory_uri(); ?>/img/elements/disabled-check.png) no-repeat center center/cover;
            border: none;
        }
        .primary-radio input:checked + label {
            background: url(<?php echo get_template_directory_uri(); ?>/img/elements/primary-radio.png) no-repeat center center/cover;
            border: none;
        }

    </style>
    <?php wp_head(); ?>
</head>
<body>

<header id="header" id="home">
    <div class="container">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="index.html"><?php if ( function_exists( 'the_custom_logo' ) ) {
                        the_custom_logo();
                    } ?></a>
            </div>
            <nav id="nav-menu-container">
                <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'header_top_menu',
                            'container_class' => 'main-navigation',
                            'items_wrap' => '<ul class="nav-menu ">%3$s</ul>',
                        )
                    );
                ?>
            </nav><!-- #nav-menu-container -->
        </div>
    </div>
</header><!-- #header -->