<?php get_header(); ?>


<!-- start banner Area -->
	<?php
	$category="banner";
	$cate = new WP_Query(array(
		'post_type'        => 'post',
		'post_per_page'    => 3,
		'orderby'          => 'title',
		'order'            => 'DESC',
		'category_name'    => $category
	));

	if( have_posts() ) : $cate->the_post();
	?>
			<section class="banner-area" id="home" style="background: url(<?php the_post_thumbnail_url('about_us', array('class' => 'post-thumb')); ?>) center;
				background-size: cover;">
				<div class="container">
					<div class="row  d-flex align-items-center justify-content-center banner" >
						<div class="banner-content col-lg-7">
							<h1>
								<?php the_title(); ?>
							</h1>
							<p class="pt-20 pb-20">
								<?php the_excerpt(); ?>
							</p>
						</div>											
					</div>
				</div>
			</section>
	<?php endif; ?>
			<!-- End banner Area -->	

			<!-- Start we-offer Area -->
<?php
	$category='Our Offered Services';
	$cat_id=get_cat_ID( $category );
?>
			<section class="we-offer-area section-gap" id="offer">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-10">
							<div class="title text-center">
								<h1 class="mb-10"><?php echo $category; ?></h1>
								<p><?php echo category_description($cat_id); ?></p>
							</div>
						</div>
					</div>


					<div class="row">
						<?php
						$cate = new WP_Query(array(
							'post_type'        => 'post',
							'post_per_page'    => 2,
							'orderby'          => 'title',
							'order'            => 'DESC',
							'category_name'    => $category
						));

						if(have_posts()): while($cate->have_posts()) : $cate->the_post();
						?>
						<div class="col-lg-6">
							<div class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('home_post', array('class' => 'post-thumb')); ?></a>
								</div>
								<div class="desc">
									<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
									<p>
										<?php the_excerpt(); ?>
									</p>
								</div>
							</div>
						</div>
						<?php
							endwhile;
							endif;
						?>
					</div>
				</div>	
			</section>
			<!-- End we-offer Area -->


			<!-- Start home-video Area -->
			<section class="home-video-area" id="about">
				<div class="container-fluid">
					<?php
					query_posts( array( 'post_type' => 'special-post-items', 'slider_cat' => 'tutorial' ) );
					//the loop start here
					if ( have_posts() ) :  have_posts(); the_post();
						$large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'special-post-items');
					?>
					<div class="row justify-content-end align-items-center">
						<div class="col-lg-4 no-padding video-right">
							<p class="top-title"><?php echo get_post_meta($post->ID, 'meta-subtitle-slider', true);?></p>
							<h1><?php the_title(); ?></h1>
							<p><span><?php echo get_post_meta($post->ID, 'meta-subtitle-slider-black', true); ?></span></p>
							<?php  the_content(); ?>
						</div>
						<section class="video-area col-lg-6" style="padding: 200px 0 200px 0; position: relative; background: url(<?php echo $large_image_url[0]; ?>) no-repeat center center/cover;">
							<div class="overlay overlay-bg"></div>
							<div class="container">
								<div class="video-content">
									<a href="<?php echo get_post_meta($post->ID, 'youtube-link', true); ?>" class="play-btn"><img src="<?php echo get_template_directory_uri(); ?>/img/play-btn.png" alt=""></a>
								</div>
							</div>
						</section>											
					</div>
					<?php  endif; wp_reset_query(); ?>
				</div>	
			</section>
			<!-- End home-aboutus Area -->


			<!-- Start home-aboutus Area -->
			<section class="home-aboutus-area">
				<div class="container-fluid">
					<div class="row justify-content-center align-items-center">
						<?php
						query_posts( array( 'post_type' => 'special-post-items', 'slider_cat' => 'support' ) );
						//the loop start here
						if ( have_posts() ) :  have_posts(); the_post();
						$large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'special-post-items');
						?>
						<div class="col-lg-8 no-padding about-left">
							<img class="img-fluid" src="<?php echo $large_image_url[0]; ?>" alt="">
						</div>
						<div class="col-lg-4 no-padding about-right">
							<p class="top-title"><?php echo get_post_meta($post->ID, 'meta-subtitle-slider', true); ?></p>
							<h1 class="text-white"><?php the_title(); ?><br></h1>
							<p><span><?php echo get_post_meta($post->ID, 'meta-subtitle-slider-black', true); ?></span></p>
							<p>
								<?php the_content(); ?>
							</p>
						</div>
						<?php  endif; wp_reset_query(); ?>
					</div>
				</div>	
			</section>
			<!-- End home-aboutus Area -->
			

			<!-- Start protfolio Area -->
			<section class="protfolio-area section-gap" id="project">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-10">
							<div class="title text-center">
								<h1 class="mb-10">Photo Gallery</h1>
								<p>Who are in extremely love with eco friendly system.</p>
							</div>
						</div>
					</div>						
					<div class="row">
						<div class="col-lg-8 single-portfolio">
						  <img class="image img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/p1.jpg" alt="">
						  <a href="<?php echo get_template_directory_uri(); ?>/img/p1.jpg" class="img-pop-up">	
							  <div class="middle">
							    <div class="text"><span class="lnr lnr-frame-expand"></span></div>
							  </div>
						  </a>
						</div>
						<div class="col-lg-4 single-portfolio">
						  <img class="image img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/p2.jpg" alt="">
						  <a href="<?php echo get_template_directory_uri(); ?>/img/p2.jpg" class="img-pop-up">	
							  <div class="middle">
							    <div class="text"><span class="lnr lnr-frame-expand"></span></div>
							  </div>
						  </a>
						</div>
						<div class="col-lg-4 single-portfolio">
						  <img class="image img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/p3.jpg" alt="">
						  <a href="<?php echo get_template_directory_uri(); ?>/img/p3.jpg" class="img-pop-up">	
							  <div class="middle">
							    <div class="text"><span class="lnr lnr-frame-expand"></span></div>
							  </div>
						  </a>
						</div>
						<div class="col-lg-8 single-portfolio">
						  <img class="image img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/p4.jpg" alt="">
						  <a href="<?php echo get_template_directory_uri(); ?>/img/p4.jpg" class="img-pop-up">	
							  <div class="middle">
							    <div class="text"><span class="lnr lnr-frame-expand"></span></div>
							  </div>
						  </a>
						</div>
						<div class="col-lg-6 single-portfolio">
						  <img class="image img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/p5.jpg" alt="">
						  <a href="<?php echo get_template_directory_uri(); ?>/img/p5.jpg" class="img-pop-up">	
							  <div class="middle">
							    <div class="text"><span class="lnr lnr-frame-expand"></span></div>
							  </div>
						  </a>
						</div>
						<div class="col-lg-6 single-portfolio">
						  <img class="image img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/p6.jpg" alt="">
						  <a href="<?php echo get_template_directory_uri(); ?>/img/p6.jpg" class="img-pop-up">	
							  <div class="middle">
							    <div class="text"><span class="lnr lnr-frame-expand"></span></div>
							  </div>
						  </a>
						</div>						
					</div>
				</div>	
			</section>
			<!-- End protfolio Area -->			

			<!-- Start callto-action Area -->
			<section class="callto-action-area relative section-gap">
				<div class="overlay overlay-bg"></div>	
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content col-lg-9">
							<div class="title text-center">
								<h1 class="mb-10 text-white">Got Impressed to our features</h1>
								<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
								<a class="primary-btn" href="#">Request Free Demo</a>
							</div>
						</div>
					</div>	
				</div>	
			</section>
			<!-- End calto-action Area -->
			
		
			<!-- Start price Area -->
			<section class="price-area section-gap" id="price">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">Choose the Perfect Plan for you</h1>
								<p>Who are in extremely love with eco friendly system.</p>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-lg-4">
							<div class="single-price">
								<div class="top-sec d-flex justify-content-between">
									<div class="top-left">
										<h4>Standard</h4>
										<p>For the <br>individuals</p>
									</div>
									<div class="top-right">
										<h1>£199</h1>
									</div>
								</div>
								<div class="bottom-sec">
									<p>
										“Few would argue that, despite the advancements
									</p>
								</div>
								<div class="end-sec">
									<ul>
										<li>2.5 GB Free Photos</li>
										<li>Secure Online Transfer Indeed</li>
										<li>Unlimited Styles for interface</li>
										<li>Reliable Customer Service</li>
										<li>Manual Backup Provided</li>
									</ul>
									<button class="primary-btn price-btn mt-20">Purchase Plan</button>
								</div>								
							</div> 
						</div>
						<div class="col-lg-4">
							<div class="single-price">
								<div class="top-sec d-flex justify-content-between">
									<div class="top-left">
										<h4>Business</h4>
										<p>For the <br>small Company</p>
									</div>
									<div class="top-right">
										<h1>£399</h1>
									</div>
								</div>
								<div class="bottom-sec">
									<p>
										“Few would argue that, despite the advancements
									</p>
								</div>
								<div class="end-sec">
									<ul>
										<li>2.5 GB Free Photos</li>
										<li>Secure Online Transfer Indeed</li>
										<li>Unlimited Styles for interface</li>
										<li>Reliable Customer Service</li>
										<li>Manual Backup Provided</li>
									</ul>
									<button class="primary-btn price-btn mt-20">Purchase Plan</button>
								</div>								
							</div> 
						</div>	
						<div class="col-lg-4">
							<div class="single-price">
								<div class="top-sec d-flex justify-content-between">
									<div class="top-left">
										<h4>Ultimate</h4>
										<p>For the <br>large Company</p>
									</div>
									<div class="top-right">
										<h1>£499</h1>
									</div>
								</div>
								<div class="bottom-sec">
									<p>
										“Few would argue that, despite the advancements
									</p>
								</div>
								<div class="end-sec">
									<ul>
										<li>2.5 GB Free Photos</li>
										<li>Secure Online Transfer Indeed</li>
										<li>Unlimited Styles for interface</li>
										<li>Reliable Customer Service</li>
										<li>Manual Backup Provided</li>
									</ul>
									<button class="primary-btn price-btn mt-20">Purchase Plan</button>
								</div>								
							</div> 
						</div>							
																						
					</div>
				</div>	
			</section>
			<!-- End price Area -->
<?php
$category='Testimonial from Clients';
$cat_id=get_cat_ID( $category );
?>
			<!-- Start testomial Area -->
			<section class="testomial-area section-gap">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10"><?php echo $category; ?></h1>
								<p><?php echo get_custom_cat_ID($category); echo category_description($cat_id); ?></p>
							</div>
						</div>
					</div>						
					<div class="row">
						<div class="active-tstimonial-carusel">
				<?php
				query_posts( array( 'post_type' => 'testimonial', 'slider_cat' => 'Testimonial from Clients' ) );
				//the loop start here
				if ( have_posts() ) : while( have_posts()) : the_post();
				$large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'testimonial');
				?>
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo $large_image_url[0]; ?>" alt="">
								<p class="desc">
									<?php the_content(); ?>
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									<?php echo get_post_meta($post->ID, 'designation', true); ?>
								</p>
							</div>
				<?php endwhile; endif; wp_reset_query(); ?>
						</div>
					</div>
				</div>	
			</section>
			<!-- End testomial Area -->
<?php
$category='Latest News from our Blog';
$cat_id=get_cat_ID( $category );
?>
			<!-- Start latest-blog Area -->
			<section class="latest-blog-area section-gap" id="blog">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10"><?php echo $category; ?></h1>
								<p><?php echo category_description($cat_id); ?></p>
							</div>
						</div>
					</div>					
					<div class="row">
						<?php
						$cate = new WP_Query(array(
							'post_type'        => 'post',
							'post_per_page'    => 2,
							'orderby'          => 'title',
							'order'            => 'DESC',
							'category_name'    => $category
						));

						if(have_posts()): while($cate->have_posts()) : $cate->the_post();
						?>
						<div class="col-lg-6 single-blog">
							<img class="img-fluid" src="<?php the_post_thumbnail_url('about_us', array('class' => 'img-fluid')); ?>" alt="">
							<ul class="tags">
								<li><a href="#">Travel</a></li>
								<li><a href="#">Life style</a></li>
							</ul>
							<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
							<p>
								<?php the_excerpt(); ?>
							</p>
							<p class="post-date"><?php echo date('d M, Y',get_post_time()); ?></p>
						</div>
						<?php
						endwhile;
						endif;
						?>
					</div>
				</div>	
			</section>
			<!-- End latest-blog Area -->


<?php get_footer(); ?>



